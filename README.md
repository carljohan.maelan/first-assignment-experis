# First Assignment experis

**The Komputer Store has functionality to:**
**Work:**
Work for 100kr per click which will be stored onto your "pay account", where you
can either deposit your pay using the 'Bank' button to your bank account in full if you have no loaned amount.
Otherwise 10% of the pay will be taken to pay off your loan and the remaining amount gets
deposited to your bankaccount. 

**Get a Loan:**
Apply to get a loan from the bank! You can only have one loan at a time and you can only
loan up to 100% of your bank balance.

**Repay Loan:**
This button only appears if you have a loaned amount AND money on your "pay account", this 
button will take the full amount from your "pay account" and pay off the amount from your loan.

**Laptops:**
Dropdown menu to browse the available laptops and display their titles, descriptions, prices,
specifications and images. Once you've decided which one you want you can purchase it using
the 'Buy Now' button aslong as you have enough in your bankaccount! 
Otherwise you will be alerted that you need to work more!


Carl-Johan Maelan
