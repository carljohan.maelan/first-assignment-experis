const computersElement = document.getElementById('computers');
const featuresElement = document.getElementById('features');
const laptopNamesElement = document.getElementById('laptopName')
const laptopSpecsElement = document.getElementById('laptopSpecs')
const computerPriceElement = document.getElementById('computerPrice')
const computerImageElement = document.getElementById('laptopImage')
const purchaseLaptopElement = document.getElementById('buyComputerButton')
const balanceElement = document.getElementById('balance');
const bankMoneyElement = document.getElementById('bankMoney');
const loanButtonElement = document.getElementById('loanButton');
const bankButtonElement = document.getElementById('bankButton');
const workMoneyElement = document.getElementById('workMoney');
const workButtonElement = document.getElementById('workButton');
const repayButtonElement = document.getElementById('repayButton');
const outstandingLoanElement = document.getElementById('outstandingLoan');
const highlightedComputerElement = document.createElement('li');
const laptopHighlightTitleElement = document.createElement('h2')
const laptopHighlightSpecsElement = document.createElement('p');
const laptopHighlightPriceElement = document.createElement('p');
const laptopImageElement = document.createElement('img');

//variables for changing integers and a const for work
let balance = 0.0;
let loanBalance = 0.0;
let pay = 0.0;
let laptopPurchase;
const work = 100; 

//fetch computers from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => computers = data)
.then(computers => addToComputers(computers));

//foreach to add computers from API
const addToComputers = (computers) => {
    computers.forEach(x => addLaptop(x));
}
//creates options for laptops to dropdown menu
const addLaptop = (laptop) => {
    const laptopElement = document.createElement('option');
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    computersElement.appendChild(laptopElement);
}

//removes previous description and presents the current selected description aswell as the
//specs, image and price
const handleComputerDropDown = () => {
    highlightedComputerElement.remove();
    const selectedComputer = computers[computersElement.selectedIndex]; 
    laptopPurchase = selectedComputer;
    highlightedComputerElement.innerText = selectedComputer.description;
    featuresElement.appendChild(highlightedComputerElement);
    laptopHighlightTitleElement.innerText = selectedComputer.title;
    laptopNamesElement.appendChild(laptopHighlightTitleElement);
    laptopHighlightSpecsElement.innerText = selectedComputer.specs;
    laptopSpecsElement.appendChild(laptopHighlightSpecsElement);
    laptopHighlightPriceElement.innerText = selectedComputer.price + ' kr';
    computerPriceElement.appendChild(laptopHighlightPriceElement);
    laptopImageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`;
    if(laptopImageElement.src === 'https://noroff-komputer-store-api.herokuapp.com/assets/images/5.jpg'){
        let pngNotJpg = laptopImageElement.src.replace('jpg', 'png');
        laptopImageElement.src = pngNotJpg;
    }
    computerImageElement.appendChild(laptopImageElement);
}
//Update balances in the accounts after a purchase or alert the user that more money is needed
const handleLaptopPurchase = () => {
    if(balance >= laptopPurchase.price){
        balance -= laptopPurchase.price;
        bankMoneyElement.innerText = `${balance + ' kr'}`;
        alert(`You are now the proud owner of: ${laptopPurchase.title} !`)
    }
    else{
        alert(`Insufficient funds! You need to work to earn: ${laptopPurchase.price - balance}`)
    }

}

//Adds 100kr each click as work
const addWorkAmount = () => {
    pay += work;
    workMoneyElement.innerText = `${pay + 'kr'}`
    hideRepayButton();
}

//Banks the amount from work to the bank
const bankMoneyAmount = () => {
    if(loanBalance > 1){
        loanBalance -= pay * 0.10;
        balance += pay * 0.9;
        if(loanBalance < 0){
            balance += pay * 0.10;
            loanBalance = 0;
        } 
        pay = 0;
        bankMoneyElement.innerText = `${balance + ' kr'}`;
        outstandingLoanElement.innerText = `Loan: ${loanBalance + ' kr'}`;
        workMoneyElement.innerText = `${pay}`;
        hideRepayButton();
    }
    else{
        balance += pay;
        pay = 0;
        bankMoneyElement.innerText = `${balance + ' kr'}`
        workMoneyElement.innerText = `${pay}`;
        hideRepayButton();
    }
}

//Hide/Show repayLoanButton
const hideRepayButton = () => {
    if(pay > 0 && loanBalance > 0){
        repayButtonElement.style.visibility="visible";
    }
    else{
        repayButtonElement.style.visibility="hidden";
    }
}
//Repay full loan amount and if the amount goes below 0 it's transferred to bankbalance
const repayLoanAmount = () => {
      loanBalance -= pay;
      if (loanBalance < 0) {
        balance += loanBalance * -1;
        loanBalance = 0;
      }
    pay = 0;
   bankMoneyElement.innerText = `${balance + " kr"}`;
   workMoneyElement.innerText = `${pay+'kr'}`
   outstandingLoanElement.innerText = `Loan: ${loanBalance+' kr'}`
   hideRepayButton();
}

//Gets a loan depending on how much you have in the bank
const getLoanAmount = () => {
    const allowedAmount = balance * 2;
    const loanPrompt = prompt("Enter the amount you wish to loan:");
    if(loanPrompt > allowedAmount){
alert("You tried to loan too much or you have a loan already that needs to be paid off")
    }
    else if(loanBalance < 1){
            loanBalance = parseInt(loanPrompt);
            balance += loanBalance;
            outstandingLoanElement.innerText = `Loan: ${loanBalance + ' kr'}`
            bankMoneyElement.innerText =`${parseInt(balance) + ' kr'}`
    }
}

//Eventlisteners for different events
workButtonElement.addEventListener("click", addWorkAmount);
bankButtonElement.addEventListener("click", bankMoneyAmount);
repayButtonElement.addEventListener("click", repayLoanAmount);
loanButtonElement.addEventListener("click", getLoanAmount);
computersElement.addEventListener("click", handleComputerDropDown);
purchaseLaptopElement.addEventListener("click", handleLaptopPurchase);

